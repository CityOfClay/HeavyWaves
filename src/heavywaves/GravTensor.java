package heavywaves;

import java.util.ArrayList;

/**
 *
 * @author MultiTool
 */
public class GravTensor extends ArrayList<Double> {// Length of this vector is number of spatial dimensions.
  public double GravScalar = 0.0;// or call them ScalarMassField and VectorMassField?
  public void Add(GravTensor other) {
    int NDims = this.size();// Math.min(this.size(), other.size());// wrong way. if other has more dims, self needs to increase dims too.
    if (NDims != other.size()) {
      System.out.println("Vector size mismatch in GravTensor.Add.");
      System.exit(1);
    }
    for (int cnt = 0; cnt < NDims; cnt++) {
      double VectEnergy, AbsEnergy = 0;
      AbsEnergy = Math.abs(this.get(cnt)) + Math.abs(other.get(cnt));
      VectEnergy = this.get(cnt) + other.get(cnt);
      this.GravScalar += AbsEnergy - VectEnergy;// All energy lost to opposing vectors becomes apparent mass.
      this.set(cnt, VectEnergy);
    }
  }
}
/*
So in the big picture, each cell queries its influences by
GravTensor sum = 0;
for each RoutingRef nbr {
. get nbr, and its distance from me
. go to nbr's history and look up event time for my distance
. that event contains the nbr state at that time. its gravtensor will have a scalar of 0, pure vector energy.
. add that event state vector to my sum.
}
now the sum is the time field I am reacting in.
change my wave time rate based on the vector and scalar of that sum field. 

should the scalar effect be full energy, or half the energy, 
since we only react to the half headed the opposite way?


*/